using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public float maxHealth = 100f;
    public Image Healthbar;
    public float CurrentHealth
    {
        get; private set; 
    }
    // Start is called before the first frame update
    void Start()
    {
        CurrentHealth = maxHealth;
    }

  public bool OnDamage(float amount)
    {
        if(CurrentHealth > 0)
        {
            CurrentHealth -= amount;
            if(CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                OnDeath();
                
            }
            Healthbar.fillAmount = CurrentHealth / maxHealth;
            return true;
        }
        return false;
    }   
    private void OnDeath()
    {
        Debug.Log("Lol imagine dying noob");
    }
}


