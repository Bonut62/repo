number = 1 #integer definition
print(number)
number = number + 1.0 #float is added to an integer and so the final result is an integer
print(number) 
#conversion to string
number = "Number:" + str(number)
print(number)
#method definition
def sum(a, b):
    return float(a + b)

#result = sum (2, 3)
#if(result != 5): #needs colon for if statement, "!" means not equal to
   # print("Fu")
    #elif(result == 10):
    #print("Bar")

message = input("Prompt: ")
message_len = len(message)

if(message_len > 0):
    if(message_len >= 30):
               print("Message cannot be longer than 29 characters")
    elif(message_len < 30):
           print(message)
    if("Yes" in message):
       print("Found 'Yes' in" + message)
    else:
        print("Found 'Yes' not in" + message) 
