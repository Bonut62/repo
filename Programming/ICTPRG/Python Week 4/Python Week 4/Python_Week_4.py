import random #import rng


def check_range(num, min, max): #check range function
   if(num >= min and num <= max):
       print("Integer is within defined range")
       return True
   print("Integer is out of defined range")
   return False

print("0. Rock")
print("1. Paper")
print("2. scissors")
user_selection = input("Enter a number from 0 to 2\n")
while(not user_selection.isnumeric()):
   user_selection = input("Enter a number\n")
user_selection = int(user_selection)
while(check_range(int(user_selection), 0, 2) == False) :
    user_selection = input("Enter a number from  0 to 2\n")

if(user_selection == 0):
    print("Player selected 'Rock'")
elif(user_selection == 1):
    print("Player selected 'Paper'")
elif(user_selection == 2):
    print("Player selected 'Scissors'")

computer_selection = random.randrange(0, 3)
print(computer_selection)
if(computer_selection == 0):
    print("Computer selected 'Rock'")
elif(computer_selection == 1):
    print("Computer selected 'Paper'")
elif(computer_selection == 2):
    print("Computer selected 'Scissors'")
